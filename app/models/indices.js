import DS from 'ember-data';


const {
    Model, 
    attr
} = DS

export default Model.extend({
	name: attr('string'),
	cod: attr('string'),
	last: attr('number'),
	variation: attr('number')
});
