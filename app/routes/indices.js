import Route from '@ember/routing/route';
import { get } from '@ember/object'

export default Route.extend({
	model(){
	return [{
		name: 'NASDAQ',
		cod: 'NDQ',
		last: 10,
		variation: 20
	},
	{
		name: 'BOVESPA',
		cod: 'IBOV',
		last: 15,
		variation: 30
	}]
	//  { 
	// return get(this, "store").findAll('users')
	// 	.then(( indice ) => {
	// 		return indice;
	// 	});
	// }
	}
});
