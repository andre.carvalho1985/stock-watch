import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | minha-carteira', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:minha-carteira');
    assert.ok(route);
  });
});
